package domain;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import org.apache.commons.lang3.StringUtils;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.UUID;

public class ClienteService {


    private MongoCollection<Document> collection;

    public ClienteService() {
        var mongoClient = new MongoClient(
                new MongoClientURI("mongodb://localhost:27017"));

        var db = mongoClient.getDatabase("vv-workshop");
        collection = db.getCollection("clientes");

    }

    public void inserirCliente(Cliente cliente) throws Exception {

        if (StringUtils.isEmpty(cliente.getNome())) {
            throw new Exception("Nome inválido");
        }

        if (StringUtils.isEmpty(cliente.getEmail())) {
            throw new Exception("Email inválido");
        }

        if (StringUtils.isEmpty(cliente.getTelefone())) {
            throw new Exception("Telefone inválido");
        }

        var document = new Document()
                .append("nome", cliente.getNome())
                .append("email", cliente.getEmail())
                .append("telefone", cliente.getTelefone())
                .append("_id", cliente.getId());

        collection.insertOne(document);

    }

    public Cliente retornarCliente(UUID id) {


        var filter = new Document()
                .append("_id", id);

        var document = collection.find(filter).first();

        var cliente = new Cliente();
        cliente.setEmail(document.getString("email"));
        cliente.setTelefone(document.getString("telefone"));
        cliente.setNome(document.getString("nome"));
        cliente.setId((UUID) document.get("_id"));

        return cliente;

    }
}
