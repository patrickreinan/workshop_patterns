package domain;


public class SimulacaoPrecoService {

    public static final int dinheiro = 1;
    public static final int boleto = 2;
    public static final int cartao = 3;


    public double simular(int valor, int formaPagamento) {

        var desconto = 0.0;
        switch (formaPagamento) {
            case dinheiro:
                desconto = 0.10;
                break;
            case boleto:
                desconto = 0.20;
                break;
            case cartao:
                desconto = 0.15;
                break;

        }

        return valor -= valor * desconto;

    }
}
