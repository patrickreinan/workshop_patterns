import com.github.javafaker.Faker;
import domain.Cliente;
import domain.ClienteService;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClienteServiceTests {

    private final Faker faker;

    public ClienteServiceTests() {
faker = new Faker();
    }

    @Test
    public void inserir_cliente_com_sucesso() throws Exception {

        var cliente = new Cliente();
        cliente.setNome(faker.name().fullName());
        cliente.setEmail(faker.internet().emailAddress());
        cliente.setTelefone((faker.phoneNumber().cellPhone()));
        cliente.setId(UUID.randomUUID());


        var service = new ClienteService();
        service.inserirCliente(cliente);

        var clienteInserido = service.retornarCliente(cliente.getId());

        assertEquals(cliente.getEmail(), clienteInserido.getEmail());
        assertEquals(cliente.getNome(), clienteInserido.getNome());
        assertEquals(cliente.getTelefone(), clienteInserido.getTelefone());
        assertEquals(cliente.getId(), clienteInserido.getId());

    }


    @Test
    public void inserir_cliente_sem_nome_com_falha() throws Exception {

        var cliente = new Cliente();
        cliente.setEmail(faker.internet().emailAddress());
        cliente.setTelefone((faker.phoneNumber().cellPhone()));
        cliente.setId(UUID.randomUUID());

        try {
            var service = new ClienteService();
            service.inserirCliente(cliente);
        }catch(Exception ex)
        {
            assertTrue(ex.getMessage().equals("Nome inválido"));
        }


    }

}
