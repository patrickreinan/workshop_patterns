import domain.SimulacaoPrecoService;
import org.bson.assertions.Assertions;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SimulacaoPrecoTests {

    @Test
    public void simular_preco_forma_pagamento_dinheiro(){

        var service = new SimulacaoPrecoService();
        var formaPagamento = SimulacaoPrecoService.dinheiro;
        var valor = 100;
        var valorEsperado  = 90.0;

        var resultado = service.simular(valor, formaPagamento);

        assertTrue(resultado==valorEsperado);


    }

    @Test
    public void simular_preco_forma_pagamento_cartao(){

        var service = new SimulacaoPrecoService();
        var formaPagamento = SimulacaoPrecoService.cartao;
        var valor = 100;
        var valorEsperado  = 85.0;

        var resultado = service.simular(valor, formaPagamento);

        assertTrue(resultado==valorEsperado);


    }

    @Test
    public void simular_preco_forma_pagamento_boleto(){

        var service = new SimulacaoPrecoService();
        var formaPagamento = SimulacaoPrecoService.boleto;
        var valor = 100;
        var valorEsperado  = 80.0;

        var resultado = service.simular(valor, formaPagamento);

        assertTrue(resultado==valorEsperado);


    }
}
