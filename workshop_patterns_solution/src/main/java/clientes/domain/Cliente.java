package clientes.domain;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.UUID;

public class Cliente {
    private UUID id;
    private String nome;
    private String telefone;
    private String email;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void validate() throws Exception {
        if (StringUtils.isEmpty(telefone)) {
            throw new Exception("Nome inválido");
        }

        if (StringUtils.isEmpty(email)) {
            throw new Exception("Email inválido");
        }

        if (StringUtils.isEmpty(telefone)) {
            throw new Exception("Telefone inválido");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(id, cliente.id) &&
                Objects.equals(nome, cliente.nome) &&
                Objects.equals(telefone, cliente.telefone) &&
                Objects.equals(email, cliente.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, telefone, email);
    }
}