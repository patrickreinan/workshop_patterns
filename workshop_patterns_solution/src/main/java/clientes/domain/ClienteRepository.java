package clientes.domain;

import java.util.UUID;

public interface ClienteRepository {
    void insert(Cliente cliente);
    Cliente get(UUID id);
}
