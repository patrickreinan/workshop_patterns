package clientes.domain;

import java.util.UUID;

public class ClienteService {

    private ClienteRepository repository;

    //DIP
    public ClienteService(ClienteRepository repository) {
        this.repository = repository;
    }

    public void inserirCliente(Cliente cliente) throws Exception {
        cliente.validate();
        repository.insert(cliente);
    }

    public Cliente retornarCliente(UUID id) {
        return repository.get(id);
    }
}

