package clientes.repository;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import clientes.domain.Cliente;
import clientes.domain.ClienteRepository;

import java.util.UUID;

public class ClienteMongoDbRepository implements ClienteRepository {

    private MongoCollection<Document> collection;

    public ClienteMongoDbRepository() {
        var mongoClient = new MongoClient(
                new MongoClientURI("mongodb://localhost:27017"));

        var db = mongoClient.getDatabase("vv-workshop");
        collection = db.getCollection("clientes");
    }


    @Override
    public void insert(Cliente cliente) {
        var document = new Document()
                .append("nome", cliente.getNome())
                .append("email", cliente.getEmail())
                .append("telefone", cliente.getTelefone())
                .append("_id", cliente.getId());

        collection.insertOne(document);
    }

    @Override
    public Cliente get(UUID id) {
        var filter = new Document()
                .append("_id", id);

        var document = collection.find(filter).first();

        var cliente = new Cliente();
        cliente.setEmail(document.getString("email"));
        cliente.setTelefone(document.getString("telefone"));
        cliente.setNome(document.getString("nome"));
        cliente.setId((UUID) document.get("_id"));

        return cliente;
    }
}
