package clientes.utils;

import com.github.javafaker.Faker;
import clientes.domain.Cliente;

import java.util.UUID;

//Builder
public class ClienteBuilder {

    private static ClienteBuilder instance;
    private  Faker faker;
    private  Cliente cliente;

    //Singleton
    public static ClienteBuilder getInstance() {
        if (instance == null)
            instance = new ClienteBuilder();

        return instance;

    }

    public ClienteBuilder() {
        faker = new Faker();
        cliente= new Cliente();
    }

    public  ClienteBuilder withNome() {
        cliente.setNome(faker.name().fullName());
        return ClienteBuilder.instance;
    }

    public  ClienteBuilder withEmail() {
        cliente.setEmail(faker.internet().emailAddress());
        return ClienteBuilder.instance;
    }

    public  ClienteBuilder withTelefone() {
        cliente.setTelefone(faker.phoneNumber().cellPhone());
        return ClienteBuilder.instance;
    }

    public  Cliente build() {
        return cliente;
    }

    public  ClienteBuilder withId() {
        cliente.setId(UUID.randomUUID());
        return ClienteBuilder.instance;
    }
}
