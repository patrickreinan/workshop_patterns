package preco;

import preco.strategy.SimulacaoPrecoFactory;

public class SimulacaoPrecoService {

//OCP - se precisar de uma nova forma de pagamento não precisa alterar aqui.
    public double simular(double valor, int formaPagamento) throws Exception {

        var strategy = SimulacaoPrecoFactory.getStrategy(formaPagamento);
        return strategy.calculate(valor);

    }
}
