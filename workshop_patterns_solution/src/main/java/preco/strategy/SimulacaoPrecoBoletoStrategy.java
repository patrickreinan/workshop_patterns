package preco.strategy;

public class SimulacaoPrecoBoletoStrategy extends SimulacaoPrecoStrategy {
    @Override
    public double calculate(double value) {
        return value-=value * 0.20;
    }
}
