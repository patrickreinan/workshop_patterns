package preco.strategy;

public class SimulacaoPrecoCartaoStrategy extends SimulacaoPrecoStrategy {
    @Override
    public double calculate(double value) {
        return value-=value *0.15;
    }
}
