package preco.strategy;

public class SimulacaoPrecoDinheiroStrategy extends SimulacaoPrecoStrategy {

    @Override
    public double calculate(double value) {
        return value -= value * 0.10;
    }
}
