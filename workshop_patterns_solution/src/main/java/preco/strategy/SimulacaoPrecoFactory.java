package preco.strategy;

import preco.FormaPagamento;

//Factory
public class SimulacaoPrecoFactory {

    //Strategy
    public static SimulacaoPrecoStrategy getStrategy(int formaPagamento) throws Exception {
        switch (formaPagamento) {
            case (FormaPagamento.dinheiro):
                return new SimulacaoPrecoDinheiroStrategy();
            case (FormaPagamento.cartao):
                return new SimulacaoPrecoCartaoStrategy();
            case (FormaPagamento.boleto):
                return new SimulacaoPrecoBoletoStrategy();
            default:
                throw new Exception("Strategy not found");
        }
    }


}
