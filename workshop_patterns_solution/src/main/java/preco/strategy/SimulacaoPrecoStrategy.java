package preco.strategy;

public abstract class SimulacaoPrecoStrategy {
    public abstract double calculate( double value);
}
