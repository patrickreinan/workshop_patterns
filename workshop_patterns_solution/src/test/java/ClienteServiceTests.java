import org.junit.Test;
import clientes.domain.ClienteRepository;
import clientes.domain.ClienteService;
import clientes.repository.ClienteMongoDbRepository;
import clientes.utils.ClienteBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClienteServiceTests {

    private final ClienteRepository repository;
    private final ClienteService service;

    public ClienteServiceTests() {
        repository = new ClienteMongoDbRepository();
        service = new ClienteService(repository);
    }

    @Test
    public void inserir_cliente_com_sucesso() throws Exception {

        var cliente = ClienteBuilder
                .getInstance()
                .withNome()
                .withEmail()
                .withTelefone()
                .withId()
                .build();

        service.inserirCliente(cliente);

        var clienteInserido = service.retornarCliente(cliente.getId());
        assertEquals(cliente, clienteInserido);

    }

    @Test
    public void inserir_cliente_sem_nome_com_falha() throws Exception {

        var cliente = ClienteBuilder
                .getInstance()
                .withEmail()
                .withTelefone()
                .withId()
                .build();
        try {

            service.inserirCliente(cliente);
        } catch (Exception ex) {
            assertTrue(ex.getMessage().equals("Nome inválido"));
        }

    }

}