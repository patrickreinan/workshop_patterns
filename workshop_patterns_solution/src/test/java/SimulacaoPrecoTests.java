import org.junit.Test;
import preco.FormaPagamento;
import preco.SimulacaoPrecoService;

import java.text.Normalizer;

import static org.junit.Assert.assertTrue;

public class SimulacaoPrecoTests {
    @Test
    public void simular_preco_forma_pagamento_dinheiro() throws Exception {

        var service = new SimulacaoPrecoService();
        var valor = 100.;
        var valorEsperado  = 90.0;

        var resultado = service.simular(valor, FormaPagamento.dinheiro);

        assertTrue(resultado==valorEsperado);


    }

    @Test
    public void simular_preco_forma_pagamento_cartao() throws Exception {

        var service = new SimulacaoPrecoService();
        var valor = 100;
        var valorEsperado  = 85.0;

        var resultado = service.simular(valor, FormaPagamento.cartao);

        assertTrue(resultado==valorEsperado);


    }

    @Test
    public void simular_preco_forma_pagamento_boleto() throws Exception {

        var service = new SimulacaoPrecoService();
        var valor = 100;
        var valorEsperado  = 80.0;

        var resultado = service.simular(valor, FormaPagamento.boleto);

        assertTrue(resultado==valorEsperado);


    }
}
